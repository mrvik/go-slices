package slices

import "gitlab.com/mrvik/go-slices/common"

func Map[T, R any](fn common.MapFunction[T, R], slice []T) []R {
	value := make([]R, len(slice))

	for k, v := range slice {
		value[k] = fn(v, k)
	}

	return value
}

func FlatMap[T, R any](fn common.MapFunction[T, []R], slice []T) []R {
	value := make([]R, 0, len(slice))

	for k, v := range slice {
		value = append(value, fn(v, k)...)
	}

	return value
}

func Filter[T any](fn common.Predicate[T], slice []T) []T {
	value := make([]T, 0, len(slice))

	for k, v := range slice {
		if !fn(v, k) {
			continue
		}

		value = append(value, v)
	}

	return value
}

func Find[T any](fn common.Predicate[T], slice []T) common.Optional[T] {
	for k, v := range slice {
		if fn(v, k) {
			return common.Optional[T]{
				HasValue: true,
				Value:    v,
			}
		}
	}

	return common.Optional[T]{}
}
