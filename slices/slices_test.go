package slices_test

import (
	mr "math/rand"
	"strconv"
	"testing"
	"testing/quick"

	"github.com/stretchr/testify/assert"
	"gitlab.com/mrvik/go-slices/common"
	"gitlab.com/mrvik/go-slices/slices"
)

// Test slices
var (
	slice10 = []int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
)

// Map functions
func fnNoop[T any](a T, _ int) T {
	return a
}

func fnIndex[T any](_ T, i int) int {
	return i
}

func TestMap(t *testing.T) {
	t.Parallel()

	expect := assert.New(t)

	expect.NotNil(slices.Map(fnNoop[string], []string(nil)), "Map must not return nil even on nil input")
	expect.Equal(slice10, slices.Map(fnIndex[string], make([]string, 10)))
}

func BenchmarkMap(b *testing.B) {
	field := make([]string, b.N)

	b.ReportAllocs()
	b.ResetTimer()

	_ = slices.Map(fnIndex[string], field)
}

func BenchmarkMapNoop(b *testing.B) {
	field := make([]string, b.N)

	b.ReportAllocs()
	b.ResetTimer()

	_ = slices.Map(fnNoop[string], field)
}

func TestFlatMap(t *testing.T) {
	t.Parallel()

	fmFunction := func(k int, _ int) []any {
		return []any{
			k, strconv.Itoa(k),
		}
	}
	fmNil := func(s struct{}, _ int) []struct{} {
		return nil
	}

	expect := assert.New(t)
	result := slices.FlatMap(fmFunction, slices.Map(fnIndex[struct{}], make([]struct{}, 5)))

	expect.Len(result, 10, "Result must have all returned elements")
	expect.Equal([]any{0, "0", 1, "1", 2, "2", 3, "3", 4, "4"}, result, "Result must be the expected")

	expect.NotPanics(assert.PanicTestFunc(func() {
		slices.FlatMap(fmNil, make([]struct{}, 10))
	}), "FlatMap must not panic on nil when an empty slice is returned from fn")
	expect.Len(
		slices.FlatMap(fmNil, make([]struct{}, 10)),
		0,
		"FlatMap must return 0 length array when map function returns nil",
	)
}

func doubleValue[T any](t T, idx int) []T {
	return []T{t, t}
}

func BenchmarkFlatMap(b *testing.B) {
	elements := slices.Map(fnIndex[struct{}], make([]struct{}, b.N))

	b.ReportAllocs()
	b.ResetTimer()

	_ = slices.FlatMap(doubleValue[int], elements)
}

func predicateAlways[T any](what bool) func(T, int) bool {
	return func(T, int) bool { return what }
}

func TestFilter(t *testing.T) {
	t.Parallel()

	expect := assert.New(t)
	res := slices.Filter(predicateAlways[string](false), make([]string, 100))

	expect.NotNil(res, "Not matching result must not be nil")
	expect.Len(res, 0, "Non matching result must have 0 length")

	res = slices.Filter(predicateAlways[string](true), make([]string, 100))

	expect.Len(res, 100, "Results where all match predicate must return the same number as input")
}

func BenchmarkFilterFalse(b *testing.B) {
	elements := make([]string, b.N)

	b.ReportAllocs()
	b.ResetTimer()

	slices.Filter(predicateAlways[string](false), elements)
}

func BenchmarkFilterTrue(b *testing.B) {
	elements := make([]string, b.N)

	b.ReportAllocs()
	b.ResetTimer()

	slices.Filter(predicateAlways[string](true), elements)
}

func fnEquals[T comparable](what T) common.Predicate[T] {
	return func(e T, _ int) bool {
		return e == what
	}
}

func TestFind(ext *testing.T) {
	ext.Parallel()

	qconfig := &quick.Config{
		MaxCount: 10000,
	}

	ext.Run("real find", func(sub *testing.T) {
		sub.Parallel()

		if err := quick.Check(func(haystack []string) bool {
			haystack = append(haystack, "test", "another test")

			expected := mr.Intn(len(haystack))
			found := slices.Find(fnEquals(haystack[expected]), haystack)

			return found.HasValue && found.Must() == haystack[expected]
		}, qconfig); err != nil {
			sub.Error(err)
		}
	})

	ext.Run("duck hunt", func(sub *testing.T) {
		sub.Parallel()

		if err := quick.Check(func(haystack []string) bool {
			return !slices.Find(predicateAlways[string](false), haystack).HasValue
		}, qconfig); err != nil {
			sub.Error(err)
		}
	})

	ext.Run("first ok", func(sub *testing.T) {
		sub.Parallel()

		if err := quick.Check(func(haystack []string) bool {
			res := slices.Find(predicateAlways[string](true), haystack)

			return len(haystack) == 0 || res.HasValue && res.Value == haystack[0]
		}, qconfig); err != nil {
			sub.Error(err)
		}
	})
}
