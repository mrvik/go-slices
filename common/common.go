package common

type MapFunction[T, R any] func(T, int) R

type Optional[T any] struct {
	Value    T
	HasValue bool
}

func (o Optional[T]) Must() T {
	if !o.HasValue {
		panic("no value in optional")
	}

	return o.Value
}

type Predicate[T any] func(T, int) bool
