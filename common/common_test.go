package common_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/mrvik/go-slices/common"
)

func TestMust(t *testing.T) {
	t.Parallel()

	expect := assert.New(t)

	expect.Panics(
		assert.PanicTestFunc(func() { common.Optional[string]{}.Must() }),
		"Empty Optional must panic on Must call",
	)
	expect.Equal(
		"testing value",
		common.Optional[string]{Value: "testing value", HasValue: true}.Must(),
		"Unwrapped value must be the same to the .Value field",
	)
}
