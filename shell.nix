let
  unstable = import <unstable> {};
in
{pkgs ? import <nixpkgs> {}, ...}: pkgs.mkShell {
  packages = [
    unstable.go
  ];
}
